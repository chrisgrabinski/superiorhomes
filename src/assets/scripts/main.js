'use strict';

// ==================================================
// Sticky Logo
// -----
// After scrolling, the logo becomes sticky
// ==================================================

// $(document).on('scroll', function() {
//   // TODO: Performance improvements for scroll event
//
//   var position = $(document).scrollTop();
//
//   if ( position >= 250 ) {
//     $('.header').addClass('header--sticky');
//   } else if ( position === 0 )
//   {
//     $('.header').removeClass('header--sticky');
//   }
// });

// ==================================================
// Houses Meta Navigation
// -----
// Meta Navigation slides down from top
// ==================================================

$('#button--navigation-houses').on('click', function() {
  $('body').toggleClass('navigation-houses--active');
  $(this).toggleClass('navigation__button--active');
});


jQuery(document).ready(function($){
    //function to check if the .cd-image-container is in the viewport here
    // ...

    //make the .cd-handle element draggable and modify .cd-resize-img width according to its position
    $('.cd-image-container').each(function(){
        var actual = $(this);
        drags(actual.find('.cd-handle'), actual.find('.cd-resize-img'), actual);
    });

    //function to upadate images label visibility here
    // ...
});

// ==================================================
// Image Comparison
// -----
// Placeholder Script
// ==================================================


$( ".comparison__button" ).draggable({
  axis: "x",
  containment: ".comparison",
  scroll: false,
  drag: function( event, ui ) {

    if ( ui.position.left <= 32 )
    {
      ui.position.left = 32;
      $('.comparison__slide--action').width(32);
    } else {
      var widthTotal = $('.comparison').width();
      var percentage = ui.position.left / widthTotal * 100;

      console.log(percentage);

      $('.comparison__slide--action').width(percentage + '%');
    }
  }
});
