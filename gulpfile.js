'use strict';

var gulp            = require('gulp'),
    plugin          = require('gulp-load-plugins')(),
    pngquant        = require('imagemin-pngquant'),
    bower           = require('bower-main'),
    browserSync     = require('browser-sync'),
    reload          = browserSync.reload;

// ==================================================
// Server Task
// -----
// Start Server with BrowserSync
// ==================================================

gulp.task('serve', ['html', 'styles', 'bower', 'scripts', 'images'], function() {
  browserSync({
      server: "./dist"
  });

  gulp.watch("src/assets/styles/**/*.scss", ['styles']);
  gulp.watch("src/assets/scripts/*.js", ['scripts']);
  gulp.watch("src/assets/images/*", ['images']);
  gulp.watch("./bower_components/*", ['bower']);
  gulp.watch("src/*.html", ['html']);
});

// ==================================================
// HTML Task
// -----
// Minify HTML
// ==================================================

gulp.task('html', function() {
  return gulp.src("src/**/*.html")
    .pipe(plugin.htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest("dist/"))
    .pipe(reload({stream: true}));
});

// ==================================================
// Styles Task
// -----
// Process SASS w/ vendor prefixes then minify and add sourcemaps
// ==================================================

gulp.task('styles', function() {
  return gulp.src('src/assets/styles/*.scss')
    .pipe(plugin.sourcemaps.init()) // Initiate sourcemaps
    .pipe(plugin.sass({errLogToConsole: true})) // Compile SASS
    .pipe(plugin.autoprefixer({ // Autoprefix CSS
        browsers: ['IE 8', 'IE 9', 'last 2 versions'],
        cascade: false
    }))
    .pipe(plugin.minifyCss({
      advanced: false
    }))
    .pipe(plugin.sourcemaps.write('.')) // Write sourcemaps
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(reload({stream: true}));
});

// ==================================================
// Scripts Task
// -----
// Uglify JavaScript
// ==================================================

gulp.task('scripts', function() {
  return gulp.src('src/assets/scripts/*.js')
    .pipe(plugin.uglify())
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(reload({stream: true}));
});


// ==================================================
// Bower Task
// -----
// Uglify JavaScript
// ==================================================

gulp.task('bower-js', function() {
  return gulp.src(bower('js','min.js').normal)
    .pipe(plugin.sourcemaps.init())
      .pipe(plugin.concat('vendor.js'))
      .pipe(plugin.uglify())
    .pipe(plugin.sourcemaps.write('.'))
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(reload({stream: true}));
});

gulp.task('bower-css', function() {
  return gulp.src(bower('css','min.css').normal)
    .pipe(plugin.sourcemaps.init())
      .pipe(plugin.concat('vendor.css'))
    .pipe(plugin.minifyCss())
    .pipe(plugin.sourcemaps.write('.'))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(reload({stream: true}));
});

gulp.task('bower', ['bower-js', 'bower-css']);


// ==================================================
// Images Task
// -----
// Compress images
// ==================================================

gulp.task('images', function() {
  return gulp.src('src/assets/images/*')
    .pipe(plugin.imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('dist/assets/img'))
    .pipe(reload({stream: true}));
});

// Tasks

gulp.task('default', ['serve']);
